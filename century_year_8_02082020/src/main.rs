// Link:
//https://www.codewars.com/kata/5a3fe3dde1ce0e8ed6000097

// Description:
//Given a year, return the century it is in.
//Input, Output Examples ::
//centuryFromYear(1705)  returns (18)
//centuryFromYear(1900)  returns (19)
//centuryFromYear(1601)  returns (17)
//centuryFromYear(2000)  returns (20)

//fn century(year: u32) -> u32 {}

//use std::ops::Rem;

fn main() {}

//pub fn century(year: u32) -> u32 {
//    if year < 100 {
//        1
//    } else if year.rem(100) == 0 {
//        year / 100
//    } else {
//        year / 100 + 1
//    }
//}

// To simplify, we can use match:
pub fn century(year: u32) -> u32 {
    match (year / 100, year % 100) {
        (lhs, 0) => lhs,
        (lhs, _) => lhs + 1,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_tests() {
        assert_eq!(century(1705), 18);
        assert_eq!(century(1900), 19);
        assert_eq!(century(1601), 17);
        assert_eq!(century(2000), 20);
        assert_eq!(century(89), 1);
        assert_eq!(century(101), 2);
        assert_eq!(century(1), 1);
    }
}
