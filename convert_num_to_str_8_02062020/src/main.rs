// Link:
// https://www.codewars.com/kata/5265326f5fda8eb1160004c8

// Description:
//We need a function that can transform a number into a string.
//What ways of achieving this do you know?
//Examples:
//number_to_string(123) //=> "123"
//number_to_string(999) //=> "999"

//fn number_to_string(i: i32) -> String {}

fn main() {}

pub fn number_to_string(i: i32) -> String {
    i.to_string()
}


#[cfg(test)]
mod tests {
   use super::*;

   #[test]
   fn returns_number_as_a_string() {
      assert_eq!(number_to_string(67), "67");
      assert_eq!(number_to_string(1+2), "3");
   }
}
