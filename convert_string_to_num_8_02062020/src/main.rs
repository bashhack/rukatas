// Link:
//https://www.codewars.com/kata/544675c6f971f7399a000e79

// Description:
//Note: This kata is inspired by Convert a Number to a String!. Try that one too.
//We need a function that can transform a string into a number. What ways of achieving this do you know?
//Note: Don't worry, all inputs will be strings, and every string is a perfectly valid representation of an integral number.
//Examples
//string_to_number("1234")  == 1234
//string_to_number("605")   == 605
//string_to_number("1405")  == 1405
//string_to_number("-7")    == -7

use std::str::FromStr;

fn main() {}

fn string_to_number(s: &str) -> i32 {
    s.parse::<i32>().unwrap()
}

// Alternatively, without the turbofish I could use the trait FromStr
//fn string_to_number(s: &str) -> i32 {
//    i32::from_str(s).unwrap()
//}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn returns_expected() {
        assert_eq!(string_to_number("1234"), 1234);
        assert_eq!(string_to_number("605"), 605);
        assert_eq!(string_to_number("1405"), 1405);
        assert_eq!(string_to_number("-7"), -7);
    }
}
